#-*- coding:utf-8 -*-
import os
import imp
import sys
import time
#from Cheetah.Template import Template
import json
import md5
#import redis
import logging
import traceback
import urllib
#from common import base

reload(sys)
sys.setdefaultencoding('utf8')

#rlog = base.FeimatLog("logs/sample.log")

class sample():
    def __init__(self):
        # init here
        #self.up_t = Template(file="templates/upload.html")
        pass

    def upload(self, request, response_head):
        recv_data = {}
        recv_data["code"] = 200
        if "upload_file" not in request.filedic:
            return "别攻击我"
        fileitem = request.filedic["upload_file"]
        if fileitem.filename == "":
            return "请选择文件后再上传"
        filename = str(time.time()) + fileitem.filename
        fd = open("./upstatic/%s" % filename, "w")
        fd.write(fileitem.file.read())
        fd.close()
        recv_data["url"] = "/upstatic/%s" % filename
        return json.dumps(recv_data)
